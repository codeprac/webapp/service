module.exports = {
	env: {
		browser: true,
		es2021: true,
	},
	extends: [
		'airbnb',
		'plugin:import/recommended',
		'prettier',
		'react-app',
		'react-app/jest',
	],
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		ecmaVersion: 2020,
		project: './jsconfig.json',
	},
	plugins: ['import', 'prettier', 'react'],
	rules: {
		'import/order': [
			'error',
			{
				alphabetize: {
					order: 'asc',
					caseInsensitive: true,
				},
				groups: [
					'builtin',
					'external',
					'internal',
					'index',
					'sibling',
					'parent',
				],
				'newlines-between': 'always',
			},
		],
		// this is an entirely frontend project, .js is fine
		'react/jsx-filename-extension': ['error', { extensions: ['.js'] }],
		'import/extensions': ['error', { extensions: ['.js', '.json'] }],
		// not needed since react 17
		'react/react-in-jsx-scope': 'off',
		semi: ['error', 'always'],
	},
	settings: {
		'import/resolver': {
			node: {
				paths: ['src'],
			},
		},
	},
};
