const autprefixer = require('autoprefixer');
const tailwindcss = require('tailwindcss');
const eslint = require('./.eslintrc');

module.exports = {
	eslint,
	style: {
		postcss: {
			plugins: [tailwindcss, autprefixer],
		},
	},
};
