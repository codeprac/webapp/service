import PropTypes from 'prop-types';

import FormField from 'components/FormField';
import { useAccount } from 'providers/Account';

import { FORM_FIELDS } from './data';

const AccountEditor = ({ data }) => {
	const fields = FORM_FIELDS;
	const account = useAccount();
	const handle = {
		save: async (fieldID, value) => {
			const update = await account.update(fieldID, value);
			if (update.data.code !== 'ok') {
				return update.data.data;
			}
			return true;
		},
	};
	const fieldElements = fields
		.map((field) => {
			const fieldDataExists =
				(data?.[field.id] ?? false) !== false || data?.[field.id] === null;
			if (fieldDataExists === false) {
				return null;
			}
			let fieldElement;
			switch (field.type) {
				case 'text':
				default:
					fieldElement = (
						<FormField
							id={field.id}
							handle={handle}
							label={field.label}
							type={field.type}
							validate={field.validate}
							value={data[field.id] || field.default}
						/>
					);
			}
			return <div key={field.id}>{fieldElement}</div>;
		})
		.filter((v) => v);
	return <div className="AccountEditor">{fieldElements}</div>;
};

AccountEditor.propTypes = {
	// eslint-disable-next-line react/forbid-prop-types
	data: PropTypes.object.isRequired,
};

export default AccountEditor;
