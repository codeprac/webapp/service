import isEmail from 'validator/es/lib/isEmail';

export const DEFAULT_TEXT = '';

export const FORM_FIELDS = [
	{
		id: 'username',
		isRequired: true,
		label: 'your codeprac username',
		type: 'text',
		default: DEFAULT_TEXT,
		validate: (input) => {
			const valid =
				input.match(/^[a-zA-Z0-9][a-zA-Z0-9.]+[a-zA-Z0-9]$/g) !== null;
			if (!valid) {
				return 'usernames must be alphanumeric, at least 3 characters long, and cannot end with a period';
			}
			return true;
		},
	},
	{
		id: 'email',
		isRequired: true,
		label: 'your private email address',
		type: 'text',
		default: DEFAULT_TEXT,
		validate: (input) => {
			const valid = isEmail(input);
			if (!valid) {
				return "that doesn't look like a valid email address";
			}
			return true;
		},
	},
];
