import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import CreateProfileNotice from 'components/CreateProfileNotice';
import Explainer from 'components/Explainer';
import Todo from 'components/Todo';
import ProfileEditor from 'containers/PageProfileEdit/ProfileEditor';
import routes from 'data/routes';
import { useProfile } from 'providers/Profile';

const INITIAL_STATE = {
	fields: [],
	data: null,
	error: null,
	loaded: false,
};

const EXPLAINER = `
this is where you can decide how you want to appear to the world. having an 
awesome developer profile attracts people to you. attract friends to hang,
attract peers to challenge each other, attract mentors to guide you,
or attract recruiters to profit. your profile, your call.
`;

const PageProfileEdit = () => {
	// const api = useAPI();
	const profile = useProfile();
	const [state, setState] = useState(INITIAL_STATE);
	useEffect(() => {
		if (state.loaded === false) {
			profile
				.get()
				.then(() => {
					setState({ ...state, loaded: true });
				})
				// eslint-disable-next-line no-console
				.catch(console.warn);
		}
	}, [profile, state]);
	const handleCreateProfile = async () => {
		await profile.create();
	};

	const isProfileNotFound =
		state.loaded && !profile.loading && profile.code === 'profile_not_found';
	const isProfileOK = state.loaded && !profile.loading && profile.code === 'ok';
	const isProfileHandleSet = profile.data?.handle ?? false;

	return (
		<div className="PageProfileEdit">
			<Todo>
				<h1>your profile.</h1>
				<Explainer data={EXPLAINER} />
				{isProfileNotFound && (
					<CreateProfileNotice onCreate={handleCreateProfile} />
				)}

				<br />
				<div>
					{isProfileOK && isProfileHandleSet && (
						<a
							href={`/@${profile.data.handle}`}
							target="_blank"
							rel="noreferrer"
						>
							view your profile
						</a>
					)}
					{isProfileOK && !isProfileHandleSet && (
						<span>
							<span style={{ textDecoration: 'line-through' }}>
								view your profile
							</span>
							&nbsp;
							<em>(set your handle to access this)</em>
						</span>
					)}
				</div>

				<Link to={routes.portfolio.path}>manage portfolio projects</Link>

				{isProfileOK && <ProfileEditor data={profile.data} />}
			</Todo>
		</div>
	);
};

export default PageProfileEdit;
