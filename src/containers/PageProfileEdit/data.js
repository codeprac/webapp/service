export const DEFAULT_TEXT = '';
export const DEFAULT_BOOL = false;

/**
 * FORM_FIELDS
 * -----------
 *
 * The `id` properties of these object should align with the request
 * serializer of the endpoint handling the update process.
 */
const FORM_FIELDS = [
	{
		label: 'you as a person.',
		type: 'section',
	},
	{
		label: 'preview of your profile picture',
		src: 'avatarURL',
		type: 'imagePreview',
	},
	{
		id: 'avatarURL',
		label: 'your profile image URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'handle',
		label: 'handle for this profile',
		type: 'text',
		default: DEFAULT_TEXT,
		validate: (value) => {
			if (value.length === 0) {
				return true;
			}
			if (!value.match(/^[a-z0-9]+$/)) {
				return 'handle should be alphanumeric only';
			}
			return true;
		},
	},
	{
		id: 'name',
		label: 'name for this profile',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'bio',
		label: 'a short about-you',
		type: 'textarea',
		default: DEFAULT_TEXT,
	},
	{
		id: 'technologies',
		label: 'what technologies are you familiar with?',
		type: 'textarea',
		default: DEFAULT_TEXT,
	},
	{
		id: 'learning',
		label: 'what do you want to learn?',
		type: 'textarea',
		default: DEFAULT_TEXT,
	},
	{
		id: 'fun',
		label: 'what else do you do for fun?',
		type: 'textarea',
		default: DEFAULT_TEXT,
	},
	{
		id: 'urlBlog',
		label: 'your personal blog URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'isPrivate',
		label: 'this profile should be made private',
		type: 'checkbox',
		default: DEFAULT_BOOL,
	},
	{
		id: 'passwordKey',
		label: 'password to access your profile',
		type: 'password',
		default: DEFAULT_TEXT,
		if: {
			isPrivate: true,
		},
		required: true,
	},
	// {
	// 	label: 'you as a professional.',
	// 	type: 'section',
	// },
	// {
	// 	id: 'current_position',
	// 	label: 'Your current position',
	// 	type: 'text',
	// 	default: DEFAULT_TEXT,
	// },
	// {
	// 	id: 'current_organisation',
	// 	label: 'Your current organisation/product',
	// 	type: 'text',
	// 	default: DEFAULT_TEXT,
	// },
	// {
	// 	id: 'current_organisation_url',
	// 	label: 'Organisation/product website',
	// 	type: 'text',
	// 	default: DEFAULT_TEXT,
	// },
	// {
	// 	id: 'current_status',
	// 	label: 'Encourage connections for job opportunities',
	// 	type: 'checkbox',
	// 	default: DEFAULT_BOOL,
	// },
	{
		label: 'what brings you here.',
		type: 'section',
	},
	{
		id: 'isLookingForMentorship',
		label: 'interested to be mentored by others',
		type: 'checkbox',
		default: DEFAULT_BOOL,
	},
	{
		id: 'isLookingToMentor',
		label: 'interested to be a mentor to other engineers',
		type: 'checkbox',
		default: DEFAULT_BOOL,
	},
	{
		id: 'isLookingToExplore',
		label: 'interested to see what other engineers are up to',
		type: 'checkbox',
		default: DEFAULT_BOOL,
	},
	{
		id: 'isLookingForOpportunites',
		label: 'interested to get hired by awesome organisations',
		type: 'checkbox',
		default: DEFAULT_BOOL,
	},
	{
		id: 'isLookingToHire',
		label: 'interested to hire awesome engineers',
		type: 'checkbox',
		default: DEFAULT_BOOL,
	},
	{
		id: 'isFOMO',
		label: 'just here to look-see look-see coz FOMO',
		type: 'checkbox',
		default: DEFAULT_BOOL,
	},
	{
		label: 'other profiles.',
		type: 'section',
	},
	{
		id: 'urlCodeMentor',
		label: 'your CodeMentor profile URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'urlDev',
		label: 'your DEV community URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'urlGithub',
		label: 'your Github profile URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'urlGitlab',
		label: 'your Gitlab profile URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'urlInstagram',
		label: 'your Instagram profile URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'urlLinkedIn',
		label: 'your LinkedIn profile URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'urlMedium',
		label: 'your Medium profile URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'urlStackOverflow',
		label: 'your StackOverflow profile URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'urlTwitter',
		label: 'your Twitter profile URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'urlVimeo',
		label: 'your Vimeo profile URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
	{
		id: 'urlYoutube',
		label: 'your YouTube profile URL',
		type: 'text',
		default: DEFAULT_TEXT,
	},
];

export default FORM_FIELDS;
