import PropTypes from 'prop-types';

import FormField from 'components/FormField';
import FORM_FIELDS from 'containers/PageProfileEdit/data';
import { useProfile } from 'providers/Profile';

const ProfileEditor = ({ data }) => {
	const profile = useProfile();
	const fields = FORM_FIELDS;
	const handle = {
		save: (id, value) => {
			const profileUUID = data.uuid;
			profile
				.update(profileUUID, id, value)
				// eslint-disable-next-line no-console
				.catch(console.warn);
		},
	};
	return (
		<div className="ProfileEditor">
			<form>
				{fields
					.map((field, index) => {
						if (field.if !== undefined) {
							// eslint-disable-next-line guard-for-in
							for (const key in field.if) {
								if (data[key] !== field.if[key]) {
									return null;
								}
							}
						}
						if (field.src !== undefined) {
							if (!(data?.[field.src] ?? false) && !data?.[field.src]) {
								return null;
							}
						}
						const { id, label, type, validate } = field;
						const value = data?.[field.id] ?? '';
						return (
							<div
								key={field.id || `ProfileEditor-item-${index}`}
								style={{ width: '100%' }}
							>
								{type === 'imagePreview' ? (
									<div
										className="bg-center bg-cover inline-block rounded-full"
										style={{
											backgroundImage: `url(${data[field.src]})`,
											height: '128px',
											width: '128px',
										}}
									/>
								) : (
									<FormField
										id={id}
										handle={handle}
										label={label}
										type={type}
										validate={validate}
										value={value}
									/>
								)}
							</div>
						);
					})
					.filter((v) => v)}
			</form>
		</div>
	);
};

ProfileEditor.propTypes = {
	data: PropTypes.shape({
		uuid: PropTypes.string,
	}).isRequired,
};

export default ProfileEditor;
