import { useEffect } from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';

import Todo from 'components/Todo';
import routes from 'data/routes';
import utils from 'data/utils';
import { useAPI } from 'providers/API';

const PageAuthenticateWith = () => {
	const api = useAPI();
	const history = useHistory();
	const location = useLocation();
	const { search } = location;
	const params = new URLSearchParams(search);
	const code = params.get('code');
	const state = params.get('state');
	const error = params.get('error');

	const { platform } = useParams();

	useEffect(() => {
		if (error && error.length > 0) {
			history.push(
				`${routes.error.path}?id=oauth2_failed&redirect_to=${routes.auth.path}`,
			);
			return;
		}
		api
			.authenticateWithOAuth2(platform, code, state)
			.then((sessionData) => {
				window.location.href = utils.getServerURL(`/session/${sessionData}`);
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.warn(err);
				history.push(
					`${routes.error.path}?id=${err.toString()}&redirect_to=${
						routes.auth.path
					}`,
				);
			});
	});

	return (
		<div className="PageAuthenticateWith">
			<Todo>
				<p>signing in with {platform}...</p>
			</Todo>
		</div>
	);
};

export default PageAuthenticateWith;
