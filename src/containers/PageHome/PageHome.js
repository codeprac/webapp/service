import './PageHome.css';

import Explainer from 'components/Explainer';
import PageFooter from 'components/PageFooter';
import Todo from 'components/Todo';
import SECTIONS from 'containers/PageHome/data';

const EXPLAINER = `
this is our homepage. it admittedly looks underwhelming right now, but soon my friend. soon.
`;

const PageHome = () => (
	<div className="PageHome">
		<Todo>
			<div
				aria-label="codeprac logo"
				className="logo"
				data-testid="logo"
				role="img"
				style={{
					backgroundImage: 'url(assets/logo-light-bg-1000.png)',
				}}
				title="codeprac logo"
			/>
			<Explainer data={EXPLAINER} />
			<br />
			{SECTIONS.map((section) => {
				switch (section.type) {
					case 'header':
						return <h2 key={section.id}>{section.content}</h2>;
					case 'list':
						return (
							<ol key={section.id}>
								{section.content.map((lineContent, index) => (
									/* eslint-disable-next-line react/no-array-index-key */
									<li key={`${section.id}-item-${index}`}>{lineContent}</li>
								))}
							</ol>
						);
					case 'text':
					default:
						if (typeof section.content === 'object') {
							return section.content.map((lineContent, index) => (
								// eslint-disable-next-line react/no-array-index-key
								<p key={`${section.id}-${index}`}>{lineContent}</p>
							));
						}
						return <p key={section.id}>{section.content}</p>;
				}
			})}
		</Todo>
		<PageFooter />
	</div>
);

export default PageHome;
