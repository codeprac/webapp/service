const SECTIONS = [
	{
		id: 'manifesto-text',
		type: 'text',
		content: [
			'this is for for the ones who see their code as a craft,',
			"the ones who want to ship code that doesn't just work;",
			'the ones that care about how it all ties up;',
			'the ones who believe there’s always a better way of getting things done;',
			'because there is.',
			'with practice.',
		],
	},
	{
		id: 'mission',
		type: 'header',
		content: `our mission`,
	},
	{
		id: 'mission-text',
		type: 'text',
		content: `
we want to help software engineers of all levels to level up
`,
	},
	{
		id: 'values',
		type: 'header',
		content: `our values`,
	},
	{
		id: 'values-text',
		type: 'text',
		content: [
			`
expanding knowledge through reading quality articles
		`,
			`
practicing the writing of code with different constraints
		`,
			`
learning from and providing different points of view
		`,
			`
connecting with a community of like-minded people
		`,
			`
getting to work with the best in the field
		`,
		],
	},
];

export default SECTIONS;
