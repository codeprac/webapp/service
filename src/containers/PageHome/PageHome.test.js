import { render } from '@testing-library/react';

import PageHome from './PageHome';

describe('PageHome', () => {
	let element = render(<div />);

	beforeAll(() => {
		element = render(<PageHome />);
	});

	test('logo is accessible', async () => {
		const logo = await element.findByTestId('logo');
		expect(logo.getAttribute('aria-label')).not.toBe(null);
		expect(logo.getAttribute('role')).toBe('img');
	});

	test('explainer exists', async () => {
		expect(
			await element.container.getElementsByClassName('Explainer'),
		).not.toBe(null);
	});

	test('page footer exists', async () => {
		expect(
			await element.container.getElementsByClassName('PageFooter'),
		).not.toBe(null);
	});
});
