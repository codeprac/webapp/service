import Explainer from 'components/Explainer';
import Todo from 'components/Todo';
import utils from 'data/utils';
import { useSession } from 'providers/Session';

const EXPLAINER = `
this is where you can login to codeprac, for now we require a third
party authentication provider but we'll probably use credentials soon.
`;

const PageAuthentication = () => {
	const session = useSession();
	const stateString = encodeURIComponent(generateStateString());
	const linkToSignInWithGithub = utils.getServerURL(
		`/auth/github?state=${stateString}`,
	);

	const { data, error, loading } = session;
	const isOnline =
		data === null || // not loaded yet
		(loading && !error) || // reloading
		(!loading && data !== null && !error); // loaded

	return (
		<div className="PageAuthentication">
			<Todo>
				<h1>get access.</h1>
				<Explainer data={EXPLAINER} />
				<h2>ways you can sign in/up</h2>
				{isOnline ? (
					<a href={linkToSignInWithGithub}>sign in with github</a>
				) : (
					<span>
						<span style={{ textDecoration: 'line-through' }}>
							sign in with github
						</span>
						&nbsp;<em>(server seems down)</em>
					</span>
				)}
			</Todo>
		</div>
	);
};

export default PageAuthentication;

function generateStateString() {
	return (new Date().valueOf() * Math.random()).toString(16).replace('.', '');
}
