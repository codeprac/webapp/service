import { useEffect } from 'react';

import Todo from 'components/Todo';
import utils from 'data/utils';

const PageUnauth = () => {
	useEffect(() => {
		setTimeout(() => {
			window.location.href = utils.getServerURL(`/unauth`);
		}, 1000);
	});
	return (
		<div className="PageUnauth">
			<Todo>
				<h1>bye.</h1>
			</Todo>
		</div>
	);
};

export default PageUnauth;
