import './PageProfileView.css';

import { UserIcon, LinkIcon } from '@heroicons/react/outline';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import PageFooter from 'components/PageFooter';
import Todo from 'components/Todo';
import api from 'utils/api';

import Portfolios from './Portfolios';
import ProfileInterestsList from './ProfileInterestsList';
import ProfileUrlsList from './ProfileUrlsList';

const INITIAL_STATE = {
	data: null,
	error: null,
	loaded: false,
	loading: false,
};

const PageProfileView = () => {
	const { handle } = useParams();
	const [state, setState] = useState(INITIAL_STATE);
	useEffect(() => {
		if (state.loaded === false && state.loading === false) {
			setState({ ...state, loading: true });
			api.authed
				.get(`/profile/${handle}`)
				.then((res) => {
					// eslint-disable-next-line no-console
					console.log(res);
					setState({
						...state,
						data: res.data.data,
						error: null,
						loading: false,
						loaded: true,
					});
				})
				.catch((error) => {
					// eslint-disable-next-line no-console
					console.warn(error);
					setState({
						...state,
						data: null,
						error,
						loading: false,
						loaded: true,
					});
				});
		}
	}, [handle, state]);

	const isErrored = state.error !== null;
	const isLoading = state.loading === true;
	const isLoaded = state.loaded === true && state.data !== null;

	return (
		<div className="PageProfileView">
			<Todo>
				{isErrored && <div className="errored">{state.error.toString()}</div>}
				{/* eslint-disable-next-line react/no-unescaped-entities */}
				{isLoading && <div className="loader">loading {handle}'s profile</div>}
				{isLoaded && (
					<div className="content">
						<div className="info">
							<div className="avatar">
								{state.data.avatarURL ? (
									<img
										alt={`${state.data.name}'s profile pic`}
										src={state.data.avatarURL}
									/>
								) : (
									<UserIcon className="h-24 w-24" />
								)}
							</div>

							<h1 className="handle">{state.data.handle}</h1>
							<div className="name">
								<strong>{state.data.name}</strong>
								<p className="bio">{state.data.bio}</p>
							</div>

							<div className="links">
								<ProfileUrlsList data={state.data} />
							</div>

							<div className="interests">
								<strong>iam interested in</strong>
								<a className="anchor" id="interests" href="#interests">
									<LinkIcon className="icon" />
								</a>
								<ProfileInterestsList data={state.data} />
							</div>
						</div>

						<div className="more">
							<p>
								{/* eslint-disable-next-line react/no-unescaped-entities */}
								<strong>things i'm learning</strong>
								<a className="anchor" id="learning" href="#learning">
									<LinkIcon className="icon" />
								</a>
							</p>
							<div className="learning">{state.data.learning}</div>

							<p>
								{/* eslint-disable-next-line react/no-unescaped-entities */}
								<strong>things i'm good with</strong>
								<a className="anchor" id="expertise" href="#expertise">
									<LinkIcon className="icon" />
								</a>
							</p>
							<div className="technologies">{state.data.technologies}</div>

							<p>
								<strong>things i do for fun</strong>
								<a className="anchor" id="hobbies" href="#hobbies">
									<LinkIcon className="icon" />
								</a>
							</p>
							<div className="fun">{state.data.fun}</div>
						</div>

						<div className="portfolio">
							<Portfolios profileUUID={state.data.uuid} />
						</div>
					</div>
				)}
			</Todo>
			<PageFooter />
		</div>
	);
};

export default PageProfileView;
