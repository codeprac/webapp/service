import PageProfileView from './PageProfileView';

/**
 * This is the page for viewing profiles
 */
export default PageProfileView;
