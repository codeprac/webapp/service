import './ProfileUrlsList.css';

import {
	faGithub,
	faGitlab,
	faLinkedin,
	faMedium,
	faStackOverflow,
	faTwitter,
	faDev,
	faVimeo,
	faYoutube,
} from '@fortawesome/free-brands-svg-icons';
import {
	faUserAstronaut,
	faLaptopCode,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

import ProfileUrlIcon from './ProfileUrlIcon';

const URL_MAPPING = [
	{
		component: <FontAwesomeIcon icon={faUserAstronaut} />,
		description: 'their blog',
		id: 'urlBlog',
	},
	{
		component: <FontAwesomeIcon icon={faLaptopCode} />,
		description: 'their CodeMentor profile',
		id: 'urlCodeMentor',
	},
	{
		component: <FontAwesomeIcon icon={faDev} />,
		description: 'their Dev community profile',
		id: 'urlDev',
	},
	{
		component: <FontAwesomeIcon icon={faGithub} />,
		description: 'their Github profile',
		id: 'urlGithub',
	},
	{
		component: <FontAwesomeIcon icon={faGitlab} />,
		description: 'their Gitlab profile',
		id: 'urlGitlab',
	},
	{
		component: <FontAwesomeIcon icon={faLinkedin} />,
		description: 'their LinkedIn profile',
		id: 'urlLinkedIn',
	},
	{
		component: <FontAwesomeIcon icon={faMedium} />,
		description: 'their Medium blog',
		id: 'urlMedium',
	},
	{
		component: <FontAwesomeIcon icon={faStackOverflow} />,
		description: 'their StackOverflow profile',
		id: 'urlStackOverflow',
	},
	{
		component: <FontAwesomeIcon icon={faTwitter} />,
		description: 'their Twitter profile',
		id: 'urlTwitter',
	},
	{
		component: <FontAwesomeIcon icon={faVimeo} />,
		description: 'their Vimeo profile',
		id: 'urlVimeo',
	},
	{
		component: <FontAwesomeIcon icon={faYoutube} />,
		description: 'their YouTube channel',
		id: 'urlYoutube',
	},
];

const ProfileUrlsList = ({ data }) => (
	<ul className="ProfileUrlsList">
		{URL_MAPPING.map((item) =>
			data[item.id] ? (
				<li key={item.id}>
					<ProfileUrlIcon
						component={item.component}
						link={data[item.id]}
						description={item.description}
					/>
				</li>
			) : null,
		).filter((v) => v)}
	</ul>
);

ProfileUrlsList.propTypes = {
	data: PropTypes.shape({
		urlBlog: PropTypes.string,
		urlCodeMentor: PropTypes.string,
		urlDev: PropTypes.string,
		urlGithub: PropTypes.string,
		urlGitlab: PropTypes.string,
		urlLinkedIn: PropTypes.string,
		urlMedium: PropTypes.string,
		urlStackOverflow: PropTypes.string,
		urlTwitter: PropTypes.string,
		urlVimeo: PropTypes.string,
		urlYoutube: PropTypes.string,
	}).isRequired,
};

export default ProfileUrlsList;
