import PropTypes from 'prop-types';

const ProfileUrlIcon = ({ component, description, link }) => (
	<a
		aria-label={description}
		href={link}
		target="_blank"
		rel="noreferrer"
		title={description}
	>
		{component}
	</a>
);

ProfileUrlIcon.propTypes = {
	component: PropTypes.node.isRequired,
	description: PropTypes.string.isRequired,
	link: PropTypes.string.isRequired,
};

export default ProfileUrlIcon;
