import { ThumbUpIcon } from '@heroicons/react/outline';
import PropTypes from 'prop-types';

const INTEREST_MAPPING = [
	{
		id: 'isLookingToExplore',
		label: 'learning and expanding horizons',
	},
	{
		id: 'isLookingForOpportunites',
		label: 'finding new work opportunities',
	},
	{
		id: 'isLookingToMentor',
		label: 'mentoring others',
	},
	{
		id: 'isLookingForMentorship',
		label: 'receiving mentorship',
	},
	{
		id: 'isLookingToHire',
		label: 'hiring awesome engineers',
	},
	{
		id: 'isFOMO',
		label: '(here mainly for the FOMO)',
	},
];

const ProfileInterestsList = ({ data }) => (
	<ul className="ProfileInterestsList">
		{INTEREST_MAPPING.map((interest) =>
			data[interest.id] ? (
				<li key={interest.id}>
					<ThumbUpIcon className="h-5 inline mb-1" /> {interest.label}
				</li>
			) : null,
		).filter((v) => v)}
	</ul>
);

ProfileInterestsList.propTypes = {
	data: PropTypes.shape({
		isFOMO: PropTypes.bool,
		isLookingForMentorship: PropTypes.bool,
		isLookingForOpportunites: PropTypes.bool,
		isLookingToExplore: PropTypes.bool,
		isLookingToHire: PropTypes.bool,
		isLookingToMentor: PropTypes.bool,
	}).isRequired,
};

export default ProfileInterestsList;
