import './Portfolios.css';

import { ExternalLinkIcon } from '@heroicons/react/outline';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

import Button from 'components/Button';
import { INTENT_CALL_TO_INACTION } from 'components/Button/Button';
import Card from 'components/Card';
import Repository from 'components/Repository';
import api from 'utils/api';

const INITIAL_STATE = {
	data: null,
	error: null,
	loaded: false,
	loading: false,
};

const Portfolios = ({ profileUUID }) => {
	const [state, setState] = useState(INITIAL_STATE);
	useEffect(() => {
		if (state.loaded === false && state.loading === false) {
			api.unauthed
				.get(`/profile/${profileUUID}/portfolios`)
				.then((res) => {
					// eslint-disable-next-line no-console
					console.log(res);
					setState({
						...state,
						data: res.data.data,
						error: null,
						loading: false,
						loaded: true,
					});
				})
				.catch((error) => {
					// eslint-disable-next-line no-console
					console.warn(error);
					setState({
						...state,
						data: null,
						error,
						loading: false,
						loaded: true,
					});
				});
		}
	}, [profileUUID, state]);

	const isErrored = state.error !== null;
	const isLoading = state.loading === true;
	const isLoaded = state.loaded === true && state.data !== null;

	function handleOpenPortfolioProject(url) {
		window.open(url);
	}

	return (
		<div className="Portfolios">
			<h3>my portfolio projects</h3>
			{isErrored && <div className="errored">errored</div>}
			{isLoading && <div className="loader">loading</div>}
			{isLoaded && (
				<div className="content">
					{state.data.map((portfolio) => (
						<div key={portfolio.uuid}>
							<Card>
								<Repository
									description={portfolio.description}
									follows={portfolio.followsCount}
									likes={portfolio.likesCount}
									title={`${portfolio.owner}/${portfolio.path}`}
								/>
								<Button
									intent={INTENT_CALL_TO_INACTION}
									onClick={() => handleOpenPortfolioProject(portfolio.url)}
								>
									<ExternalLinkIcon className="w-4" />
								</Button>
							</Card>
						</div>
					))}
				</div>
			)}
		</div>
	);
};

Portfolios.propTypes = {
	profileUUID: PropTypes.string.isRequired,
};

export default Portfolios;
