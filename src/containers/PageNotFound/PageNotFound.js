import Explainer from 'components/Explainer';
import Todo from 'components/Todo';

const TITLE = "looks like you're lost.";

const EXPLAINER = `
this page shows when you've been directed to a page that doesn't exist. hmmmm.
`;

const PageNotFound = () => (
	<div className="PageNotFound">
		<Todo>
			<h1>{TITLE}</h1>
			<Explainer data={EXPLAINER} />
		</Todo>
	</div>
);

export default PageNotFound;
