import { useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';

import Todo from 'components/Todo';
import errors from 'data/errors';

const PageError = () => {
	const history = useHistory();
	const location = useLocation();
	const { search } = location;
	const params = new URLSearchParams(search);
	const redirectTo = params.get('redirect_to');
	const isRedirectEnabled = !!redirectTo;
	const errorId = params.get('id');
	const errorInstance = errors[errorId];
	const message =
		errorInstance?.message ||
		`an unknown error code '${errorId}' is all we got`;

	useEffect(() => {
		setTimeout(() => {
			history.push(redirectTo);
		}, 5000);
	});

	return (
		<div className="PageError">
			<Todo>
				<div>!OH NOS!</div>
				<b>{message}</b>
				{isRedirectEnabled &&
					(() => (
						<div>
							<br />
							<small>Redirecting you in 5 seconds...</small>
						</div>
					))()}
			</Todo>
		</div>
	);
};

export default PageError;
