import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import Card from 'components/Card';
import Explainer from 'components/Explainer';
import LinkedAccount from 'components/LinkedAccount';
import Todo from 'components/Todo';
import AccountEditor from 'containers/Form/AccountEditor';
import routes from 'data/routes';
import { useAccount } from 'providers/Account';

const INITIAL_STATE = {
	data: null,
	loaded: false,
};

const TITLE = 'settings.';

const EXPLAINER = `
this page gives you access to the nitty-gritties of having accounts. things
such as email, your username, how you log in, where your codeprac account
is linked and other such stuff.
`;

const getLinkedAccountsData = (apiData) =>
	apiData.map((linkedAccount) => {
		const { platformData } = linkedAccount;
		return {
			id: linkedAccount.platformUserID,
			platform: linkedAccount.platformID,
			...JSON.parse(platformData),
		};
	});

const PageSettings = () => {
	const account = useAccount();
	const [state, setState] = useState(INITIAL_STATE);
	useEffect(() => {
		if (state.loaded === false) {
			account
				.get()
				.then(() => {
					setState({ ...state, data: account.data, loaded: true });
				})
				// eslint-disable-next-line no-console
				.catch(console.warn);
		}
	});
	let linkedAccounts = [];
	if (state.loaded && (account?.data?.linkedAccounts ?? false)) {
		linkedAccounts = getLinkedAccountsData(account.data.linkedAccounts);
	}
	return (
		<div className="PageSettings">
			<Todo>
				<h1>{TITLE}</h1>
				<Explainer data={EXPLAINER} />

				<h2>on codeprac.</h2>
				{state.loaded && account.data && <AccountEditor data={account.data} />}

				<h2>elsewhere.</h2>
				{state.loaded &&
					linkedAccounts.map((linkedAccount) => (
						<div key={`${linkedAccount.platform}-${linkedAccount.id}`}>
							<Card>
								<LinkedAccount
									data={linkedAccount}
									platform={linkedAccount.platform}
								/>
							</Card>
						</div>
					))}

				<h2>other things.</h2>
				<Link to={routes.unauth.path}>logout</Link>
			</Todo>
		</div>
	);
};

export default PageSettings;
