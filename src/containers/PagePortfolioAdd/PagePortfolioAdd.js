import './PagePortfolioAdd.css';

import { faGithub } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';

import Button from 'components/Button';
import {
	INTENT_CALL_TO_ACTION,
	INTENT_CALL_TO_INACTION,
} from 'components/Button/Button';
import Loader from 'components/Loader';
import Repository from 'components/Repository';
import Todo from 'components/Todo';
import routes from 'data/routes';
import api from 'utils/api';

const INITIAL_STATE = {
	data: null,
	loaded: false,
	loading: false,
};

const PagePortfolioAdd = () => {
	const history = useHistory();
	const [state, setState] = useState(INITIAL_STATE);
	const params = useParams();
	const { platform, owner } = params;
	const repoPath = params[0];

	useEffect(() => {
		if (state.loaded === false && state.loading !== true) {
			setState({ ...state, loading: true });
			api.authed
				.get(`/project/${platform}/${owner}/${repoPath}`)
				.then((res) => {
					const { data } = res;
					setState({ ...state, data: data.data, loaded: true, loading: false });
				})
				// eslint-disable-next-line no-console
				.catch(console.warn);
		}
	}, [owner, params, platform, repoPath, setState, state]);

	const isLoading = state.loading === true;
	const isLoaded = state.loaded === true && state.data !== null;

	async function handleCancel() {
		history.goBack();
	}

	async function handleConfirm() {
		const { uuid, platformID, platformProjectID } = state.data;
		const url = '/profile/portfolio';
		const data = {
			uuid,
			platformID,
			platformProjectID,
		};
		try {
			const res = await api.authed.post(url, data);
			if (res.data.code === 'ok') {
				history.push(`${routes.profile.path}`);
			}
		} catch (err) {
			// eslint-disable-next-line no-console
			console.warn(err);
		}
	}

	let platformIcon;
	switch (state?.data?.platformID) {
		case 'github':
		default:
			platformIcon = faGithub;
	}

	return (
		<div className="PagePortfolioAdd">
			<Todo>
				<h1>add repo?</h1>
				{isLoading && <Loader />}
				{isLoaded && (
					<div className="content">
						<FontAwesomeIcon size="3x" icon={platformIcon} />
						<Repository
							centered
							title={`${state.data.owner}/${state.data.path}`}
							likes={state.data.likesCount}
							follows={state.data.followsCount}
							description={state.data.description}
						/>
						<div className="actions">
							<div className="flex-0">
								<Button intent={INTENT_CALL_TO_INACTION} onClick={handleCancel}>
									cancel
								</Button>
							</div>
							<div className="flex-0">
								<Button intent={INTENT_CALL_TO_ACTION} onClick={handleConfirm}>
									confirm
								</Button>
							</div>
						</div>
					</div>
				)}
			</Todo>
		</div>
	);
};

export default PagePortfolioAdd;
