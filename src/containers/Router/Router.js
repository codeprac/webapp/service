import PropTypes from 'prop-types';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import NavigationMenu from 'components/NavigationMenu';
import PageAuthenticateWith from 'containers/PageAuthenticateWith';
import PageAuthentication from 'containers/PageAuthentication';
import PageDashboard from 'containers/PageDashboard';
import PageError from 'containers/PageError';
import PageHome from 'containers/PageHome';
import PageNotFound from 'containers/PageNotFound';
import PagePortfolioAdd from 'containers/PagePortfolioAdd';
import PagePortfolioEdit from 'containers/PagePortfolioEdit';
import PageProfileEdit from 'containers/PageProfileEdit';
import PageProfileView from 'containers/PageProfileView';
import PageSettings from 'containers/PageSettings';
import PageUI from 'containers/PageUI';
import PageUnauth from 'containers/PageUnauth';
import routes from 'data/routes';

const Router = ({
	auth,
	authWith,
	dashboard,
	demo,
	error,
	home,
	logout,
	notFound,
	portfolioAdd,
	portfolioEdit,
	profileEdit,
	profileView,
	settings,
} = {}) => (
	<BrowserRouter>
		<NavigationMenu />
		<Switch>
			<Route path={routes.home.path} exact>
				{home}
			</Route>
			<Route path="/@:handle" exact>
				{profileView}
			</Route>
			<Route path={`${routes.auth.path}/:platform`}>{authWith}</Route>
			<Route path={routes.auth.path}>{auth}</Route>
			<Route path={routes.error.path}>{error}</Route>
			<Route path={routes.dashboard.path}>{dashboard}</Route>
			<Route path={`${routes.portfolioAdd.path}/:platform/:owner/*`}>
				{portfolioAdd}
			</Route>
			<Route path={routes.portfolio.path}>{portfolioEdit}</Route>
			<Route path={routes.profile.path}>{profileEdit}</Route>
			<Route path={routes.settings.path}>{settings}</Route>
			<Route path={routes.unauth.path}>{logout}</Route>
			<Route path="/ui">{demo}</Route>
			<Route>{notFound}</Route>
		</Switch>
	</BrowserRouter>
);

Router.propTypes = {
	auth: PropTypes.node,
	authWith: PropTypes.node,
	dashboard: PropTypes.node,
	demo: PropTypes.node,
	error: PropTypes.node,
	home: PropTypes.node,
	logout: PropTypes.node,
	notFound: PropTypes.node,
	portfolioAdd: PropTypes.node,
	portfolioEdit: PropTypes.node,
	profileEdit: PropTypes.node,
	profileView: PropTypes.node,
	settings: PropTypes.node,
};

Router.defaultProps = {
	auth: <PageAuthentication />,
	authWith: <PageAuthenticateWith />,
	dashboard: <PageDashboard />,
	demo: <PageUI />,
	error: <PageError />,
	home: <PageHome />,
	logout: <PageUnauth />,
	notFound: <PageNotFound />,
	portfolioAdd: <PagePortfolioAdd />,
	portfolioEdit: <PagePortfolioEdit />,
	profileEdit: <PageProfileEdit />,
	profileView: <PageProfileView />,
	settings: <PageSettings />,
};

export default Router;
