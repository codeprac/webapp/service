import PropTypes from 'prop-types';

import LinkedAccount from 'components/LinkedAccount';
import Selector from 'components/Selector';

const LinkedAccountSelector = ({ linkedAccounts, onSelect }) => {
	const linkedAccountsSelectorData = linkedAccounts.map((linkedAccount) => {
		const linkedAccountData = JSON.parse(linkedAccount.platformData);
		return {
			content: (
				<LinkedAccount
					data={{
						avatarURL: linkedAccountData.avatarURL,
						id: linkedAccountData.id,
						name: linkedAccountData.name,
						username: linkedAccountData.username,
					}}
					platform={linkedAccount.platformID}
				/>
			),
			id: linkedAccount.uuid,
			instance: linkedAccount,
		};
	});

	return (
		<div className="LinkedAccountSelector">
			<h2>add a project</h2>
			<p>
				select one of your linked accounts to add a project from that account
			</p>
			<Selector onSelect={onSelect} selections={linkedAccountsSelectorData} />
		</div>
	);
};

LinkedAccountSelector.propTypes = {
	linkedAccounts: PropTypes.arrayOf(
		PropTypes.shape({
			uuid: PropTypes.string.isRequired,
			platformData: PropTypes.string.isRequired,
			platformID: PropTypes.string.isRequired,
		}),
	).isRequired,
	onSelect: PropTypes.func.isRequired,
};

export default LinkedAccountSelector;
