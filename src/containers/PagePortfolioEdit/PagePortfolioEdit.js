import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import Explainer from 'components/Explainer';
import Loader from 'components/Loader';
import Todo from 'components/Todo';
import routes from 'data/routes';
import { useAccount } from 'providers/Account';
import api from 'utils/api';

import LinkedAccountSelector from './LinkedAccountSelector';
import Portfolio from './Portfolio';
import RepositorySelector from './RepositorySelector';

const REPOS_PER_PAGE = 20;

const EXPLAINER = `
this is where you can select your existing code repositories for display on your profile.
while having a portfolio is not strictly necessary, having one lets people know which
of your 
`;

const INITIAL_STATE = {
	loaded: false,
	reposLoading: false,
	currentLinkedAccount: {
		platform: null,
		uuid: null,
	},
};

const PagePortfolioEdit = () => {
	const history = useHistory();
	const account = useAccount();
	const [state, setState] = useState(INITIAL_STATE);
	useEffect(() => {
		if (state.loaded === false) {
			account
				.get()
				.then(() => {
					setState({ ...state, loaded: true });
				})
				// eslint-disable-next-line no-console
				.catch(console.warn);
		}
	}, [account, state]);

	const isLinkedAccountsOK = account?.data?.linkedAccounts ?? false;
	const linkedAccounts = isLinkedAccountsOK
		? account.data.linkedAccounts
		: null;

	async function handleLoadMoreRepositories() {
		const { platform, uuid } = state.currentLinkedAccount;
		setState({ ...state, reposLoading: true });
		const page = Math.floor(state.data.length / REPOS_PER_PAGE) + 1;
		const limit = REPOS_PER_PAGE;
		const response = await api.authed.get(`/account/linked/${uuid}/projects`, {
			params: { page, platform, limit },
		});
		if (response.status === 200) {
			let data = [].concat(state.data);
			data = data.concat(response.data.data);
			setState({ ...state, data, reposLoading: false });
		}
	}

	async function handleSelectLinkedAccount(platform, uuid) {
		const currentLinkedAccount = { ...state.currentLinkedAccount };
		currentLinkedAccount.platform = platform;
		currentLinkedAccount.uuid = uuid;
		setState({
			...state,
			currentLinkedAccount,
			reposLoading: true,
		});
		const page = 1;
		const limit = REPOS_PER_PAGE;
		const response = await api.authed.get(`/account/linked/${uuid}/projects`, {
			params: { page, platform, limit },
		});
		if (response.status === 200) {
			const { data } = response.data;
			setState({ ...state, currentLinkedAccount, data, reposLoading: false });
		}
	}

	async function handleSelectRepository(repository) {
		// eslint-disable-next-line no-console
		console.log('selected repository', repository);
		if (!(state?.currentLinkedAccount?.platform ?? false)) {
			// eslint-disable-next-line no-console
			console.warn('invalid state: no linked account selected');
			return;
		}
		const owner = repository.ownerUsername;
		const repo = repository.name;
		history.push(
			`${routes.portfolioAdd.path}/${state.currentLinkedAccount.platform}/${owner}/${repo}`,
		);
	}

	return (
		<div className="PagePortfolioEdit">
			<Todo>
				<h1>your portfolio.</h1>

				<Explainer data={EXPLAINER} />

				<Portfolio />

				{state.loaded && isLinkedAccountsOK && (
					<LinkedAccountSelector
						linkedAccounts={linkedAccounts}
						onSelect={(linkedAccount) =>
							handleSelectLinkedAccount(
								linkedAccount.platformID,
								linkedAccount.uuid,
							)
						}
					/>
				)}

				{state.loaded && state.data && (
					<RepositorySelector
						onLoadMore={handleLoadMoreRepositories}
						onSelect={handleSelectRepository}
						repositories={state.data}
					/>
				)}
				{state.reposLoading && <Loader />}
			</Todo>
		</div>
	);
};

export default PagePortfolioEdit;
