import PropTypes from 'prop-types';

import Repository from 'components/Repository';
import Selector from 'components/Selector';

const RepositorySelector = ({ onLoadMore, onSelect, repositories }) => {
	const repositoriesData = repositories.map((repository) => ({
		id: repository.id,
		instance: repository,
		content: (
			<Repository
				title={repository.fullName}
				likes={repository.stars}
				follows={repository.watchers}
				description={repository.description}
			/>
		),
	}));
	return (
		<div className="RepositorySelector">
			<h2>which repo?</h2>
			<p>
				select one of your repositories to continue on to adding it as a
				portfolio project
			</p>
			<Selector onSelect={onSelect} selections={repositoriesData} />
			<button onClick={onLoadMore} type="submit">
				load more please
			</button>
		</div>
	);
};

RepositorySelector.propTypes = {
	repositories: PropTypes.arrayOf(
		PropTypes.shape({
			description: PropTypes.string.isRequired,
			fullName: PropTypes.string.isRequired,
			id: PropTypes.string.isRequired,
			url: PropTypes.string.isRequired,
		}),
	).isRequired,
	onLoadMore: PropTypes.func.isRequired,
	onSelect: PropTypes.func.isRequired,
};

export default RepositorySelector;
