import './Portfolio.css';

import { useEffect, useState } from 'react';

import Card from 'components/Card';
import Repository from 'components/Repository';
import api from 'utils/api';

const INITIAL_STATE = {
	data: null,
	error: null,
	loaded: false,
	loading: false,
};

const Portfolio = () => {
	const [state, setState] = useState(INITIAL_STATE);
	useEffect(() => {
		if (!state.loaded && !state.loading) {
			setState({ ...state, loading: true });
			api.authed
				.get('/profile/portfolios')
				.then((res) => {
					setState({
						...state,
						data: res.data.data,
						loaded: true,
						loading: false,
					});
				})
				.catch((error) => {
					// eslint-disable-next-line no-console
					console.warn(error);
					setState({
						...state,
						data: null,
						error,
						loaded: true,
						loading: false,
					});
				});
		}
	}, [state]);

	const isDataAvailable = state.data !== null;
	const isErrored = state.error !== null;

	return (
		<div className="Portfolio">
			<h2>current projects</h2>
			{isErrored && <div className="error">something went wrong :/</div>}
			{isDataAvailable && (
				<div className="content">
					{state.data.map((project) => (
						<div className="project" key={project.uuid}>
							<Card>
								<Repository
									title={`${project.owner}/${project.path}`}
									likes={project.likesCount}
									follows={project.followsCount}
									description={project.description}
								/>
							</Card>
						</div>
					))}
				</div>
			)}
		</div>
	);
};

export default Portfolio;
