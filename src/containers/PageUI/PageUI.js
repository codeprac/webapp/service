import Button from 'components/Button';
import {
	INTENT_CALL_TO_ACTION,
	INTENT_CALL_TO_INACTION,
	INTENT_TAKE_NOTE_AH,
	INTENT_DANGER,
} from 'components/Button/Button';
import Card from 'components/Card';
import FormCheckBox from 'components/FormCheckBox';
import FormTextArea from 'components/FormTextArea';
import FormTextField from 'components/FormTextField';
import Todo from 'components/Todo';

const PageUI = () => (
	<div className="PageUI">
		<Todo>
			<h1>Typography</h1>
			<hr />
			<h1>Sample h1 text</h1>
			<span>sample span to demo spacing</span>
			<h2>Sample h2 text</h2>
			<span>sample span to demo spacing</span>
			<h3>Sample h3 text</h3>
			<span>sample span to demo spacing</span>
			<h4>Sample h4 text</h4>
			<span>sample span to demo spacing</span>
			<h5>Sample h5 text</h5>
			<div>
				<p>
					Sample paragraph 1: Excepteur consectetur magna enim fugiat et dolor
					mollit mollit nostrud est est deserunt pariatur. Ullamco exercitation
					duis pariatur deserunt dolore nisi. Esse cupidatat velit id ipsum
					consectetur exercitation. Labore aute culpa labore reprehenderit
					dolore.
				</p>
				<p>
					Sample paragraph 2: Elit nostrud aliquip exercitation excepteur.
					Occaecat tempor sit ullamco magna sint pariatur veniam incididunt anim
					aliquip cillum ad. Aute nostrud officia aliquip minim commodo amet
					consequat ad dolore in nisi. Nulla nulla occaecat consequat aliquip
					nostrud ex esse officia ex ipsum. Dolore laborum sit et sint ut ipsum
					qui est labore ad cillum commodo eu aute. Consequat consequat sint
					nisi cillum excepteur in.
				</p>
				<p>
					Sample paragraph 3: Tempor labore magna officia Lorem non cupidatat
					minim consequat eiusmod amet proident. Proident do eiusmod voluptate
					deserunt deserunt nostrud proident qui aliquip. Officia consectetur
					anim commodo consequat consequat Lorem ex.
				</p>
			</div>
			<div>
				<sub>Sample subscript text</sub>
			</div>
			<div>
				<sup>Sample superscript text</sup>
			</div>
			<div>
				<small>Sample small text</small>
			</div>
			<div>
				<i>Sample italicised text</i>
			</div>
			<div>
				<em>Sample emphasised text</em>
			</div>
			<div>
				<b>Sample bolded text</b>
			</div>
			<div>
				<strong>Sample strong text</strong>
			</div>
			<div>
				<u>Sample underlined text</u>
			</div>
			<div>
				{/* eslint-disable-next-line jsx-a11y/no-distracting-elements */}
				<marquee>Sample marqueed text</marquee>
			</div>
			<div>
				{/* eslint-disable-next-line jsx-a11y/no-distracting-elements */}
				<blink>Sample blinking text</blink>
			</div>
			<div>
				<a href="?">Sample link element</a>
			</div>

			<h1>Native HTML elements</h1>
			<hr />
			<div>
				<ul>
					<li>Unordered list item 1</li>
					<li>Unordered list item 2</li>
					<li>Unordered list item 3</li>
				</ul>
			</div>
			<div>
				<ol>
					<li>Ordered list item 1</li>
					<li>Ordered list item 2</li>
					<li>Ordered list item 3</li>
				</ol>
			</div>
			<div>
				<label htmlFor="sample-checkbox">
					<input id="sample-checkbox" type="checkbox" />
					Sample checkbox input
				</label>
			</div>
			<div>
				<div>
					<label htmlFor="sample-radio-1">
						<input id="sample-radio-1" name="sample-radio" type="radio" />
						Sample radio input 1
					</label>
				</div>
				<div>
					<label htmlFor="sample-radio-2">
						<input id="sample-radio-2" name="sample-radio" type="radio" />
						Sample radio input 2
					</label>
				</div>
				<div>
					<label htmlFor="sample-radio-3">
						<input id="sample-radio-3" name="sample-radio" type="radio" />
						Sample radio input 3
					</label>
				</div>
			</div>
			<div>
				<select id="sample-dropdown">
					<option disabled selected value>
						Sample dropdown field
					</option>
					<option id="option-1" value="1">
						Sample dropdown option 1
					</option>
					<option id="option-2" value="2">
						Sample dropdown option 2
					</option>
					<option id="option-3" value="3">
						Sample dropdown option 3
					</option>
				</select>
			</div>
			<div>
				<input
					id="sample-text"
					type="text"
					placeholder="Sample text field input"
				/>
			</div>
			<div>
				<textarea
					id="sample-textarea"
					type="text"
					placeholder="Sample long text input"
				/>
			</div>
			<div>
				<input
					id="sample-password"
					type="password"
					placeholder="Sample password input"
				/>
			</div>
			<div>
				<input
					id="sample-url"
					type="url"
					placeholder="Sample url field input"
				/>
			</div>
			<div>
				<input
					id="sample-number"
					type="number"
					placeholder="Sample number field input"
				/>
			</div>
			<div>
				<input
					id="sample-tel"
					type="tel"
					placeholder="Sample telephone field input"
				/>
			</div>
			<div>
				<label htmlFor="sample-date">
					Sample date field input
					<input
						id="sample-date"
						type="date"
						placeholder="Sample date field input"
					/>
				</label>
			</div>
			<div>
				<label htmlFor="sample-range">
					Sample range field input
					<input
						id="sample-range"
						type="range"
						placeholder="Sample range field input"
					/>
				</label>
			</div>
			<div>
				<button type="button">Sample button element</button>
			</div>

			<h1>custom components</h1>

			<h2>buttons</h2>
			<div className="grid grid-cols-3 grid-rows-4">
				<Button disabled>plain disabled button</Button>
				<Button>plain button</Button>
				<Button outline>plain outline button</Button>

				<Button intent={INTENT_CALL_TO_ACTION} disabled>
					disabled call to action button
				</Button>
				<Button intent={INTENT_CALL_TO_ACTION}>call to action button</Button>
				<Button intent={INTENT_CALL_TO_ACTION} outline>
					call to action outline button
				</Button>

				<Button intent={INTENT_CALL_TO_INACTION} disabled>
					disabled call to inaction button
				</Button>
				<Button intent={INTENT_CALL_TO_INACTION}>
					call to inaction button
				</Button>
				<Button intent={INTENT_CALL_TO_INACTION} outline>
					call to inaction outline button
				</Button>

				<Button intent={INTENT_TAKE_NOTE_AH} disabled>
					disabled warning button
				</Button>
				<Button intent={INTENT_TAKE_NOTE_AH}>warning button</Button>
				<Button intent={INTENT_TAKE_NOTE_AH} outline>
					warning outline button
				</Button>

				<Button intent={INTENT_DANGER} disabled>
					disabled danger button
				</Button>
				<Button intent={INTENT_DANGER}>danger button</Button>
				<Button intent={INTENT_DANGER} outline>
					danger outline button
				</Button>
			</div>

			<h2>cards</h2>
			<Card>This is a card</Card>

			<h2>form components</h2>
			<FormTextArea />
			<FormTextField label="type=default" />
			<FormTextField label="type=password" type="password" />
			<FormCheckBox />
		</Todo>
	</div>
);

export default PageUI;
