import { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

import CreateProfileNotice from 'components/CreateProfileNotice';
import Explainer from 'components/Explainer';
import Todo from 'components/Todo';
import routes from 'data/routes';
import { useProfile } from 'providers/Profile';

const EXPLAINER = `
your dashboard is where you can find a feed of curated articles from the world
of software development together with status updates from the codeprac community.
use this page to level up via expanding your knowledge
`;

const PageDashboard = () => {
	const profile = useProfile();
	const history = useHistory();
	const [state, setState] = useState({ loaded: false });
	useEffect(() => {
		if (!state.loaded) {
			profile
				.get()
				.then(() => {
					setState({ ...state, loaded: true });
				})
				// eslint-disable-next-line no-console
				.catch(console.warn);
		}
	}, [profile, state]);
	const handleCreateProfile = async () => {
		await profile.create();
		history.push(`${routes.profile.path}`);
	};

	return (
		<div className="PageDashboard">
			<Todo>
				<h1>dashboard.</h1>
				<Explainer data={EXPLAINER} />
				<h2>new here?</h2>
				{state.loaded &&
					!profile.loading &&
					profile.code === 'profile_not_found' && (
						<CreateProfileNotice onCreate={handleCreateProfile} />
					)}
				{state.loaded && profile.data !== null && (
					<div>
						<Link to={`${routes.profile.path}`}>edit your profile</Link>
					</div>
				)}
				<h2>newsfeed.</h2>
				<p>a shameful TODO lies here</p>
			</Todo>
		</div>
	);
};

export default PageDashboard;
