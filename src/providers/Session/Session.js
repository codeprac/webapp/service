import PropTypes from 'prop-types';
import { createContext, useContext, useState } from 'react';

import api from 'utils/api';

const sessionContext = createContext();

export const useSession = () => useContext(sessionContext);

export default function SessionProvider({ children }) {
	const store = useSessionProvider();
	return (
		<sessionContext.Provider value={store}>{children}</sessionContext.Provider>
	);
}

SessionProvider.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
};

const INITIAL_STATE = {
	code: null,
	data: null,
	error: null,
	loading: false,
};

function useSessionProvider() {
	const [state, setState] = useState(INITIAL_STATE);
	return {
		...state,
		get: async (force) => {
			if (state.loading === true) {
				throw new Error('already loading');
			} else if (state.data !== null && state.error !== null && !force) {
				return;
			}

			setState({ ...state, loading: true });
			let response;
			try {
				response = await api.authed.get('/session');
			} catch (err) {
				if (err.response) {
					response = err.response;
				} else {
					response = {
						data: {
							code: err.code,
							data: undefined,
							error: err.toString(),
						},
					};
				}
			}
			const { code, data, error } = response.data;
			setState({
				...state,
				code,
				data,
				error,
				loading: false,
			});
		},
	};
}
