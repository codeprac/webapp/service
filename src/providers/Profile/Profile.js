import PropTypes from 'prop-types';
import { createContext, useContext, useState } from 'react';

import api from 'utils/api';

const profileContext = createContext();

export const useProfile = () => useContext(profileContext);

export default function ProfileProvider({ children }) {
	const store = useProfileProvider();
	return (
		<profileContext.Provider value={store}>{children}</profileContext.Provider>
	);
}

ProfileProvider.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
};

const INITIAL_STATE = {
	code: null,
	data: null,
	error: null,
	loading: false,
};

function useProfileProvider() {
	const [state, setState] = useState(INITIAL_STATE);
	return {
		...state,
		create: async () => {
			let response;
			try {
				response = await api.authed.post('/profile');
			} catch (err) {
				response = err.response;
			}
			const { code, data, error } = response.data;
			setState({
				code,
				data,
				error,
			});
		},
		get: async () => {
			if (state.loading === true) {
				throw new Error('already loading');
			} else if (state.data !== null || state.error !== null) {
				return;
			}
			setState({ ...state, loading: true });
			let response;
			try {
				response = await api.authed.get('/profile');
			} catch (err) {
				response = err.response;
			}
			const { code, data, error } = response.data;
			setState({
				...state,
				code,
				data,
				error,
				loading: false,
			});
		},
		update: async (profileUUID, fieldID, value) => {
			let response;
			try {
				response = await api.authed.put(`/profile/${profileUUID}`, {
					[fieldID]: value,
				});
			} catch (err) {
				response = err.response;
			}
			const { code, data } = response.data;
			if (response.status === 200 && code === 'ok') {
				const next = { ...state };
				for (const updatedFieldID in data) {
					if (
						updatedFieldID === fieldID &&
						next.data[updatedFieldID] !== undefined
					) {
						next.data[updatedFieldID] = data[updatedFieldID];
					}
				}
				setState(next);
			}
		},
	};
}
