import PropTypes from 'prop-types';
import { createContext, useContext, useState } from 'react';

import api from 'utils/api';

/**
 * @typedef {Object} API
 * @property {Object} Account
 * @property {Object} LinkedAccounts
 * @property {Object} Profile
 * @property {Object} Session
 * @property {Function} authenticateWithOAuth2
 * @property {Function} createProfile
 * @property {Function} getAccount
 * @property {Function} getLinkedAccounts
 * @property {Function} getProfile
 * @property {Function} getProfileByHandle
 * @property {Function} getSession
 * @property {Function} ping
 * @property {Function} updateProfile
 */

const apiContext = createContext();

export default function APIProvider({ children }) {
	const apiStore = useAPIProvider();
	return <apiContext.Provider value={apiStore}>{children}</apiContext.Provider>;
}

APIProvider.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
};

/**
 * @returns {API}
 */
export const useAPI = () => useContext(apiContext);

const INITIAL_STATE = {
	serverStatus: { data: null, loading: false, error: null },
};

function useAPIProvider() {
	const [state, setState] = useState(INITIAL_STATE);
	return {
		...state,
		authenticateWithOAuth2: async (platform, oauth2code, oauth2state) => {
			let response;
			try {
				response = await api.unauthed.get(
					`/auth/${platform}?code=${oauth2code}&state=${oauth2state}`,
				);
			} catch (err) {
				response = err.response;
			}
			const { code, data } = response.data;
			if (response.status === 200 && code === 'ok') {
				return data;
			}
			throw new Error(code);
		},
		ping: async () => {
			let response;
			try {
				response = await api.unauthed.get('/', {
					timeout: 5000,
				});
			} catch (err) {
				response = err.response;
			}
			const status = (response?.status ?? false) === 200;
			if (state.serverStatus.data !== status) {
				setState({
					...state,
					serverStatus: {
						data: status,
						loading: false,
						error: null,
					},
				});
			}
		},
	};
}
