import PropTypes from 'prop-types';
import { createContext, useContext, useState } from 'react';

import api from 'utils/api';

const accountContext = createContext();

export const useAccount = () => useContext(accountContext);

export default function AccountProvider({ children }) {
	const store = useAccountProvider();
	return (
		<accountContext.Provider value={store}>{children}</accountContext.Provider>
	);
}

AccountProvider.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
};

const INITIAL_STATE = {
	code: null,
	data: null,
	error: null,
	loading: false,
};

function useAccountProvider() {
	const [state, setState] = useState(INITIAL_STATE);
	return {
		...state,
		get: async () => {
			if (state.loading === true) {
				throw new Error('already loading');
			} else if (state.data !== null || state.error !== null) {
				return;
			}

			setState({ ...state, loading: true });
			let response;
			try {
				response = await api.authed.get('/account');
			} catch (err) {
				response = err.response;
			}

			const { code, data, error } = response.data;
			setState({
				...state,
				code,
				data,
				error,
				loading: false,
			});
		},
		update: async (fieldID, value) => {
			let response;
			try {
				response = await api.authed.put(`/account`, {
					[fieldID]: value,
				});
			} catch (err) {
				if (err?.response ?? false) {
					throw err;
				}
				response = err.response;
			}
			const { code, data } = response.data;
			if (response.status === 200 && code === 'ok') {
				const next = { ...state };
				for (const updatedFieldID in data) {
					if (
						updatedFieldID === fieldID &&
						next.data[updatedFieldID] !== undefined
					) {
						next.data[updatedFieldID] = data[updatedFieldID];
					}
				}
				setState(next);
			}
			return response;
		},
	};
}
