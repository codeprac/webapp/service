import routesData from './data';

import utils from '../utils';

const routes = utils.reduceArrayDataWithID({
	data: routesData,
	requiredFields: ['id', 'path'],
});

export default routes;
