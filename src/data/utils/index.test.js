import utils from '.';

test('parses data correctly', () => {
	const one = { id: 'a', field: '1' };
	const two = { id: 'b', field: '1' };
	const three = { id: 'c', field: '1' };
	const data = [one, two, three];
	const requiredFields = ['id', 'field'];
	const expected = {
		a: one,
		b: two,
		c: three,
	};
	expect(utils.reduceArrayDataWithID({ data, requiredFields })).toEqual(
		expected,
	);
});
