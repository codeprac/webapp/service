const SERVER_URL = process.env.REACT_APP_SERVER_BASE_URL;

const getServerURL = (forPath) => new URL(forPath, SERVER_URL);

const reduceArrayDataWithID = ({ data = [], requiredFields = [] } = []) => {
	const output = {};
	if (!(data instanceof Array)) {
		throw new Error('data should be of type Array');
	}
	if (!(requiredFields instanceof Array)) {
		throw new Error('requiredFields should be of type Array');
	}
	data.forEach((entry) => {
		if (
			requiredFields.reduce(
				(pass, field) => pass && entry[field] !== undefined,
				true,
			)
		) {
			output[entry.id] = entry;
		} else {
			// eslint-disable-next-line no-console
			console.warn(
				`excluded following entry (one or more required fields [${requiredFields.join(
					', ',
				)}] were not found)`,
				entry,
			);
		}
	});
	return output;
};

const exports = { getServerURL, reduceArrayDataWithID };

export default exports;
