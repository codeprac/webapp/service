import errorsData from './data';

import utils from '../utils';

const errors = utils.reduceArrayDataWithID({
	data: errorsData,
	requiredFields: ['id', 'message'],
});

export default errors;
