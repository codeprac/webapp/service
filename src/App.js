import './App.css';

import ServerStatusBar from 'components/ServerStatusBar';
import SessionMaintainer from 'components/SessionMaintainer';
import Router from 'containers/Router';
import AccountProvider from 'providers/Account';
import APIProvider from 'providers/API';
import ProfileProvider from 'providers/Profile';
import SessionProvider from 'providers/Session';

function App() {
	return (
		<div
			className="App"
			data-testid="app"
			style={{
				backgroundImage: 'url(assets/background.jpg)',
			}}
		>
			<APIProvider>
				<SessionProvider>
					<SessionMaintainer />
					<ServerStatusBar />
					<AccountProvider>
						<ProfileProvider>
							<Router />
						</ProfileProvider>
					</AccountProvider>
				</SessionProvider>
			</APIProvider>
		</div>
	);
}

export default App;
