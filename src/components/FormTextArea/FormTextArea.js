import PropTypes from 'prop-types';
import { useState } from 'react';

const FormTextArea = ({ id, handle, label, placeholder, validate, value }) => {
	const [state, setState] = useState({ value });
	const handleBlur = (event) => {
		handle.save(id, event.target.value);
	};
	const handleChange = (event) => {
		setState({ ...state, value: event.target.value });
	};
	return (
		<div className="FormTextArea text-left py-4">
			<label htmlFor={id}>
				<sub className="text-gray-400">{label.toUpperCase()}</sub>
				<textarea
					aria-label={label}
					id={id}
					name={id}
					placeholder={placeholder || label}
					value={state.value}
					onBlur={handleBlur}
					onChange={handleChange}
					style={{ width: '100%' }}
				/>
			</label>
		</div>
	);
};

FormTextArea.propTypes = {
	id: PropTypes.string.isRequired,
	handle: PropTypes.shape({
		save: PropTypes.func.isRequired,
	}),
	label: PropTypes.string,
	placeholder: PropTypes.string,
	validate: PropTypes.func,
	value: PropTypes.oneOfType([
		PropTypes.bool.isRequired,
		PropTypes.number.isRequired,
		PropTypes.string.isRequired,
	]),
};

FormTextArea.defaultProps = {
	handle: {
		// eslint-disable-next-line no-console
		save: console.log,
	},
	label: 'default label',
	placeholder: '',
	validate: () => true,
	value: 'default value',
};

export default FormTextArea;
