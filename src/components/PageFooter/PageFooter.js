import './PageFooter.css';
import Todo from 'components/Todo';

const PageFooter = () => (
	<div className="PageFooter">
		<hr />
		<Todo>
			<div className="grid grid-cols-2">
				<div className="text-left">
					<ul>
						<li>👩‍🚀 terms of use</li>
						<li>🕵️‍♂️ privacy policy</li>
						<li>🧭 community guidelines</li>
					</ul>
				</div>
				<div className="text-right">
					<ul>
						<li>©️ 2020-{new Date().getFullYear()} codepr.ac</li>
						<li className="text-sm">made with ❤️ in 🇸🇬</li>
					</ul>
				</div>
			</div>
		</Todo>
	</div>
);

export default PageFooter;
