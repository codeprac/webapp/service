import { render } from '@testing-library/react';

import PageFooter from './PageFooter';

describe('PageFooter', () => {
	let element = render(<div />);

	beforeEach(() => {
		element = render(<PageFooter />);
	});

	test('terms of use exists', () => {
		element.getByText(/terms of use/i);
	});

	test('privacy policy exists', () => {
		element.getByText(/privacy policy/i);
	});

	test('community guidelines exists', () => {
		element.getByText(/community guidelines/i);
	});
});
