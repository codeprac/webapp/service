import './Warn.css';

import PropTypes from 'prop-types';

const Warn = ({ children }) => (
	<span className="Warn">
		<small>⚠️ {children}</small>
	</span>
);

Warn.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.node,
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.number,
		PropTypes.string,
	]),
};

Warn.defaultProps = {
	children: '🙈 the developer forgot to put a message',
};

export default Warn;
