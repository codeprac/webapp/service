import './Error.css';

import PropTypes from 'prop-types';

const Error = ({ children }) => (
	<span className="Error">
		<small>⛔️ {children}</small>
	</span>
);

Error.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.node,
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.number,
		PropTypes.string,
	]),
};

Error.defaultProps = {
	children: '🙈 the developer forgot to put a message',
};

export default Error;
