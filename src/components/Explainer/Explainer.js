import './Explainer.css';

import PropTypes from 'prop-types';

import Card from 'components/Card';

const Explainer = ({ data }) => (
	<div className="Explainer">
		<Card>
			<strong>
				<u>EXPLAINER NOTE</u>
			</strong>
			<p>
				<small>{data}</small>
			</p>
		</Card>
	</div>
);

Explainer.propTypes = {
	data: PropTypes.string.isRequired,
};

export default Explainer;
