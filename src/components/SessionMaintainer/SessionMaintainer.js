import { useEffect, useState } from 'react';

import { useSession } from 'providers/Session';

const INTERVAL_MS = 30000;

const INITIAL_STATE = { loaded: false };

const SessionMaintainer = () => {
	const session = useSession();
	const [state, setState] = useState(INITIAL_STATE);
	useEffect(() => {
		if (state.loaded === false) {
			session
				.get()
				.then(() => {
					setState({ ...state, loaded: true });
				})
				// eslint-disable-next-line no-console
				.catch(console.warn);
		}
		const interval = setInterval(async () => {
			session.get(true);
		}, INTERVAL_MS);
		return () => clearInterval(interval);
	});
	return <div className="SessionMaintainer" style={{ display: 'none' }} />;
};

export default SessionMaintainer;
