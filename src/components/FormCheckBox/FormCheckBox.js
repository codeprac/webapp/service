import './FormCheckBox.css';

import PropTypes from 'prop-types';
import { useState } from 'react';

const FormCheckBox = ({ id, handle, label, value }) => {
	const [state, setState] = useState({ value });
	const handleChange = (event) => {
		const { checked } = event.target;
		setState({ ...state, value: checked });
		handle.save(id, checked);
	};
	return (
		<div className="FormCheckBox">
			<label htmlFor={id}>
				<input
					aria-label={label}
					id={id}
					name={id}
					type="checkbox"
					checked={state.value}
					onChange={handleChange}
				/>
				<div className="switchHolder">
					<span className="switch" />
				</div>
				{label}
			</label>
		</div>
	);
};

FormCheckBox.propTypes = {
	id: PropTypes.string.isRequired,
	label: PropTypes.string,
	value: PropTypes.oneOfType([
		PropTypes.bool.isRequired,
		PropTypes.number.isRequired,
		PropTypes.string.isRequired,
	]).isRequired,
	handle: PropTypes.shape({
		save: PropTypes.func.isRequired,
	}),
};

FormCheckBox.defaultProps = {
	handle: {
		// eslint-disable-next-line no-console
		save: console.log,
	},
	label: 'default label',
};

export default FormCheckBox;
