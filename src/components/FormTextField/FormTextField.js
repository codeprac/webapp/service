import PropTypes from 'prop-types';
import { useState } from 'react';

import Error from 'components/Label/Error';
import Warn from 'components/Label/Warn';

const INITIAL_STATE = {
	dirty: false,
	error: false,
	errorData: null,
	saving: false,
	success: false,
};

const FormTextField = ({
	id,
	handle,
	isRequired,
	label,
	placeholder,
	type,
	validate,
	value,
}) => {
	const [state, setState] = useState({
		...INITIAL_STATE,
		value,
		oldValue: value,
	});
	const isDirty = state?.dirty ?? false;
	const isEmpty = !value;
	let isValid = true;
	let invalidMessage = 'this field is invalid';
	const valid = validate(state.value);
	if (valid !== true) {
		isValid = false;
		if (typeof valid === 'string') {
			invalidMessage = valid;
		}
	}
	const shouldShowRequiredWarning = isRequired && isDirty && isEmpty;
	const shouldShowInvalidWarning = isDirty && !isValid;
	const shouldShowError = state.error === true && state.errorData !== null;

	const handleBlur = async (event) => {
		const { oldValue } = state;
		const newValue = event.target.value;
		if (newValue === oldValue) {
			return;
		}
		if (validate(newValue) !== true) {
			return;
		}

		setState({ ...state, dirty: true, saving: true });
		try {
			await handle.save(id, newValue);
		} catch (err) {
			setState({ ...state, error: true, errorData: err.toString() });
			return;
		}
		setState({
			...state,
			error: false,
			errorData: null,
			saving: false,
			oldValue: newValue,
		});
	};

	const handleChange = (event) => {
		setState({ ...state, dirty: true, value: event.target.value });
	};

	return (
		<div className="FormTextField text-left py-4">
			<label htmlFor={id}>
				<sub className="text-gray-400">{label.toUpperCase()}</sub>
				<input
					aria-label={label}
					id={id}
					name={id}
					placeholder={placeholder || label}
					type={type}
					value={state.value}
					onBlur={handleBlur}
					onChange={handleChange}
					style={{ width: '100%' }}
				/>
			</label>
			{shouldShowError && (
				<div>
					<sup>
						<Error>{state.errorData}</Error>
					</sup>
				</div>
			)}
			{shouldShowRequiredWarning && (
				<div>
					<sup>
						<Warn>this field is required</Warn>
					</sup>
				</div>
			)}
			{shouldShowInvalidWarning && (
				<div>
					<sup>
						<Warn>{invalidMessage}</Warn>
					</sup>
				</div>
			)}
		</div>
	);
};

FormTextField.propTypes = {
	id: PropTypes.string.isRequired,
	handle: PropTypes.shape({
		save: PropTypes.func.isRequired,
	}),
	isRequired: PropTypes.bool,
	label: PropTypes.string,
	placeholder: PropTypes.string,
	type: PropTypes.string.isRequired,
	validate: PropTypes.func,
	value: PropTypes.oneOfType([
		PropTypes.bool,
		PropTypes.number,
		PropTypes.string,
	]),
};

FormTextField.defaultProps = {
	handle: {
		// eslint-disable-next-line no-console
		save: console.log,
	},
	isRequired: false,
	label: 'default label',
	placeholder: '',
	validate: () => true,
	value: 'default value',
};

export default FormTextField;
