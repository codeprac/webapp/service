import './ServerStatusBar.css';

import { useSession } from 'providers/Session';

const MESSAGE_OFFLINE = 'the server has gone away, send halp';

const ServerStatusBar = () => {
	const session = useSession();
	const { data, error, loading } = session;
	const isOnline =
		data === null || // not loaded yet
		(loading && !error) || // reloading
		(!loading && data !== null && !error); // loaded

	return (
		<div className={`ServerStatusBar ${isOnline ? 'online' : 'offline'}`}>
			<div className={`status ${isOnline ? 'online' : 'offline'}`}>
				{isOnline ? 'online' : MESSAGE_OFFLINE}
			</div>
		</div>
	);
};

export default ServerStatusBar;
