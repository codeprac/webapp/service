import PropTypes from 'prop-types';

const CreateProfileNotice = ({ onCreate }) => (
	<div className="CreateProfileNotice">
		{/* eslint-disable-next-line react/no-unescaped-entities */}
		<div>You don't seem to have a profile yet</div>
		<a href="javscript:undefined" onClick={onCreate}>
			Create one now
		</a>
	</div>
);

CreateProfileNotice.propTypes = {
	onCreate: PropTypes.func.isRequired,
};

export default CreateProfileNotice;
