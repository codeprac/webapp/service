import PropTypes from 'prop-types';

import FormCheckBox from 'components/FormCheckBox';
import FormHeader from 'components/FormHeader';
import FormTextArea from 'components/FormTextArea';
import FormTextField from 'components/FormTextField';

const FormField = ({ id, handle, label, type, validate, value }) => {
	switch (type) {
		case 'section':
			return <FormHeader label={label} />;
		case 'checkbox':
			return (
				<FormCheckBox
					id={id}
					handle={handle}
					label={label}
					type={type}
					validate={validate}
					value={value}
				/>
			);
		case 'tel':
		case 'number':
		case 'password': // password uses a textfield too
		case 'text':
			return (
				<FormTextField
					id={id}
					handle={handle}
					label={label}
					type={type}
					validate={validate}
					value={value}
				/>
			);
		case 'textarea':
			return (
				<FormTextArea
					id={id}
					handle={handle}
					label={label}
					validate={validate}
					value={value}
				/>
			);
		default:
			return null;
	}
};

FormField.propTypes = {
	id: PropTypes.string,
	handle: PropTypes.shape({
		save: PropTypes.func.isRequired,
	}).isRequired,
	label: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	validate: PropTypes.func,
	value: PropTypes.oneOfType([
		PropTypes.bool.isRequired,
		PropTypes.number.isRequired,
		PropTypes.string.isRequired,
	]).isRequired,
};

FormField.defaultProps = {
	id: null,
	validate: () => true,
};

export default FormField;
