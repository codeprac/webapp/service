import './Github.css';

import { faGithub } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

const Github = ({ data }) => {
	const avatarURL =
		data.avatarURL ||
		'https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png';
	return (
		<div className="Github">
			<div
				className="avatar"
				style={{
					backgroundImage: `url(${avatarURL})`,
				}}
			>
				<div className="platform">
					<span>
						<FontAwesomeIcon size="lg" icon={faGithub} />
					</span>
				</div>
			</div>
			<div className="content">
				<div>
					<strong>@{data.username}</strong> (id: {data.id})
				</div>
				<div>&gt; {data.name}</div>
			</div>
		</div>
	);
};
Github.propTypes = {
	data: PropTypes.shape({
		avatarURL: PropTypes.string,
		bio: PropTypes.string,
		blog: PropTypes.string,
		company: PropTypes.string,
		email: PropTypes.string,
		followers: PropTypes.number,
		following: PropTypes.number,
		hireable: PropTypes.bool,
		id: PropTypes.string,
		location: PropTypes.string,
		name: PropTypes.string,
		platform: PropTypes.string,
		privateGistsCount: PropTypes.number,
		privateRepositoriesCount: PropTypes.number,
		publicGistsCount: PropTypes.number,
		publicRepositoriesCount: PropTypes.number,
		username: PropTypes.string,
	}).isRequired,
};

export default Github;
