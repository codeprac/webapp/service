import PropTypes from 'prop-types';

import Github from './Github';
import Unknown from './Unknown';

const LinkedAccount = ({ data, platform }) => {
	let Component;
	switch (platform) {
		case 'github':
			Component = Github;
			break;
		default:
			Component = Unknown;
			break;
	}
	return (
		<div className="LinkedAccount">
			<Component data={data} />
		</div>
	);
};

LinkedAccount.propTypes = {
	// ignore this because this is a switch component that redirects,
	// we cannot be sure of the structure of the data since it depends
	// on which linked account we're directing to
	//
	// eslint-disable-next-line react/forbid-prop-types
	data: PropTypes.object.isRequired,
	platform: PropTypes.string.isRequired,
};

export default LinkedAccount;
