import PropTypes from 'prop-types';

const Loader = ({ text }) => (
	<div className="Loader">
		<p>{text}</p>
	</div>
);

Loader.propTypes = {
	text: PropTypes.string,
};

Loader.defaultProps = {
	text: 'loading...',
};

export default Loader;
