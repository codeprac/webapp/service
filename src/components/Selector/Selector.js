/**
 * A selectable item in the Selector component.
 * @typedef {Object} Selection
 * @property {Object} content
 * @property {String} id
 * @property {Object} instance
 * @property {String} label
 *
 * An array of selectable items
 * @typedef {Selection[]} Selections
 *
 * Parameter object for the Selector component.
 * @typedef {Object} SelectorInput
 * @property {Function} onSelect
 * @property {Selections} selections
 */

import './Selector.css';

import PropTypes from 'prop-types';

/**
 * Selector returns a component
 *
 * @param {SelectorInput} SelectorInput
 */
const Selector = ({ onSelect, selections }) => (
	<div className="Selector">
		{selections.map((selection) => (
			<button
				key={selection.id}
				onClick={() => onSelect(selection.instance)}
				type="submit"
			>
				{selection.content || selection.label}
			</button>
		))}
	</div>
);

Selector.propTypes = {
	onSelect: PropTypes.func,
	selections: PropTypes.arrayOf(
		PropTypes.shape({
			content: PropTypes.oneOfType([
				PropTypes.node,
				PropTypes.arrayOf(PropTypes.node),
			]),
			id: PropTypes.string,
			// eslint-disable-next-line react/forbid-prop-types
			instance: PropTypes.any,
			label: PropTypes.string,
		}),
	).isRequired,
};

Selector.defaultProps = {
	onSelect: () => {
		// eslint-disable-next-line no-console
		console.warn('onSelect was not implemented');
	},
};

export default Selector;
