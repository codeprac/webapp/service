import './Repository.css';

import { UsersIcon, StarIcon } from '@heroicons/react/outline';
import PropTypes from 'prop-types';

const DEFAULT_DESCRIPTION = '[description not provided]';
const DEFAULT_TITLE = '[name not found]';
const NUMBER_NOT_SPECIFIED = -1;

const Repository = ({ centered, description, follows, likes, title }) => {
	const isCentered = centered === true;
	const repoDescription = description || DEFAULT_DESCRIPTION;
	const repoTitle = title || DEFAULT_TITLE;
	const repoFollows = `${follows >= 0 ? follows : '-'}`;
	const repoLikes = `${likes >= 0 ? likes : '-'}`;

	return (
		<div className={`Repository${isCentered ? ' centered' : ''}`}>
			<span className="title">
				<strong>{repoTitle}</strong>
			</span>
			<span className="vanity">
				<span>
					<StarIcon className="h-4 w-4" />
					<small>{repoLikes}</small>
				</span>
				<span>
					<UsersIcon className="h-4 w-4" />
					<small>{repoFollows}</small>
				</span>
			</span>
			<span className="description">
				<small>{repoDescription}</small>
			</span>
		</div>
	);
};

Repository.propTypes = {
	centered: PropTypes.bool,
	description: PropTypes.string.isRequired,
	follows: PropTypes.number,
	likes: PropTypes.number,
	title: PropTypes.string.isRequired,
};

Repository.defaultProps = {
	centered: false,
	follows: NUMBER_NOT_SPECIFIED,
	likes: NUMBER_NOT_SPECIFIED,
};

export default Repository;
