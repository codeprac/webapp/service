import './NavigationMenu.css';

import { Link } from 'react-router-dom';

import routes from 'data/routes';
import { useSession } from 'providers/Session';

const NavigationMenu = () => {
	const session = useSession();
	const menuItems = [];
	const isAuthenticated = session.data && !session.error;
	for (const [, route] of Object.entries(routes)) {
		if (route.navigationMenu !== undefined) {
			switch (route.navigationMenu.authStatus) {
				case 'all':
					menuItems.push(route);
					break;
				case 'logged_out':
					if (!isAuthenticated) {
						menuItems.push(route);
					}
					break;
				case 'logged_in':
					if (isAuthenticated) {
						menuItems.push(route);
					}
					break;
				default:
					break;
			}
		}
	}
	return (
		<div className="NavigationMenu">
			<ul>
				{menuItems.map((menuItem) => (
					<li key={menuItem.id}>
						<Link to={menuItem.path}>{menuItem.navigationMenu.label}</Link>
					</li>
				))}
			</ul>
		</div>
	);
};

export default NavigationMenu;
