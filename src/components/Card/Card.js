import './Card.css';

import PropTypes from 'prop-types';

const Card = ({ children }) => <div className="Card">{children}</div>;

Card.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.node,
		PropTypes.arrayOf(PropTypes.node),
	]).isRequired,
};

export default Card;
