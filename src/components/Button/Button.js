import './Button.css';
import PropTypes from 'prop-types';

export const INTENT_CALL_TO_ACTION = 'callToAction';
export const INTENT_CALL_TO_INACTION = 'callToInaction';
export const INTENT_DANGER = 'danger';
export const INTENT_NONE = 'none';
export const INTENT_TAKE_NOTE_AH = 'warning';

const Button = ({
	children,
	disabled,
	fullWidth,
	intent,
	outline,
	onClick,
}) => {
	const buttonClasses = [intent];
	if (fullWidth === true) {
		buttonClasses.push('fullWidth');
	}
	if (disabled === true) {
		buttonClasses.push('disabled');
	}
	if (outline === true) {
		buttonClasses.push('outline');
	}
	return (
		<div className="Button">
			<button
				className={buttonClasses.join(' ')}
				disabled={disabled}
				onClick={onClick}
				type="submit"
			>
				{children}
			</button>
		</div>
	);
};

Button.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.node,
		PropTypes.arrayOf(PropTypes.node),
	]).isRequired,
	disabled: PropTypes.bool,
	fullWidth: PropTypes.bool,
	intent: PropTypes.oneOf([
		INTENT_CALL_TO_ACTION,
		INTENT_CALL_TO_INACTION,
		INTENT_NONE,
		INTENT_TAKE_NOTE_AH,
	]),
	onClick: PropTypes.func,
	outline: PropTypes.bool,
};

Button.defaultProps = {
	disabled: false,
	fullWidth: false,
	intent: INTENT_NONE,
	// eslint-disable-next-line no-console
	onClick: console.log,
	outline: false,
};

export default Button;
