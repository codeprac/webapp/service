import './Todo.css';

import PropTypes from 'prop-types';

const Todo = ({ children }) => (
	<div className="Todo">
		<div className="container">{children}</div>
	</div>
);

Todo.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
};

export default Todo;
