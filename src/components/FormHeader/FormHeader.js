import PropTypes from 'prop-types';

const FormHeader = ({ label }) => (
	<div className="FormHeader">
		<h2>{label}</h2>
	</div>
);

FormHeader.propTypes = { label: PropTypes.string.isRequired };

export default FormHeader;
