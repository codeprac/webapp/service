import axios from 'axios';

const SERVER_URL = process.env.REACT_APP_SERVER_BASE_URL;

const unauthed = axios.create({
	withCredentials: false,
	baseURL: SERVER_URL,
});

const authed = axios.create({
	withCredentials: true,
	baseURL: SERVER_URL,
});

const api = { unauthed, authed };

export default api;
