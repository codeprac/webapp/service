image_registry := registry.gitlab.com
image_owner := codeprac
image_path := webapp/service

-include ./Makefile.properties

image_url ?= $(image_registry)/$(image_owner)/$(image_path)
release_tag ?= latest

# develop
deps:
	npm install
deps-ci:
	npm ci --cache .npm --prefer-offline
start:
	npm start
test:
	mkdir -p ./coverage
	CI=true npm test -- --coverage
lint:
	npm run lint
build:
	npm run build
image:
	docker build \
		--file ./Dockerfile \
		--tag $(image_url):latest \
		.
release: image
	docker tag $(image_url):latest $(image_url):$(release_tag)
	docker push $(image_url):$(release_tag)

# initialisation commands
this:
	npx create-react-app webapp
