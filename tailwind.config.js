function* fontSizeDegrader(minSize = 1, growthFactor) {
	let limiter = 0;
	while (true) {
		limiter += 1;
		yield limiter * growthFactor * Math.log10(limiter) + minSize;
	}
}

const fontSize = fontSizeDegrader(1, 0.25);

module.exports = {
	darkMode: false,
	purge: ['./src/**/*.{js}', './public/index.html'],
	theme: {
		extend: {},
		fontSize: {
			sm: `${fontSize.next().value}rem`,
			md: `${fontSize.next().value}rem`,
			lg: `${fontSize.next().value}rem`,
			xl: `${fontSize.next().value}rem`,
			'2xl': `${fontSize.next().value}rem`,
			'3xl': `${fontSize.next().value}rem`,
			'4xl': `${fontSize.next().value}rem`,
			'5xl': `${fontSize.next().value}rem`,
			'6xl': `${fontSize.next().value}rem`,
			'7xl': `${fontSize.next().value}rem`,
			'8xl': `${fontSize.next().value}rem`,
			'9xl': `${fontSize.next().value}rem`,
		},
	},
	variants: {
		backgroundColor: ['checked'],
		extend: {
			margin: ['first'],
		},
	},
	plugins: [],
};
