FROM node:14-alpine AS base
WORKDIR /app
COPY ./package.json ./package.json
COPY ./package-lock.json ./package-lock.json
RUN npm ci
COPY ./public ./public
COPY ./src ./src
COPY ./.eslintrc.js \
  ./craco.config.js \
  ./jsconfig.json \
  ./tailwind.config.js \
  ./
ENV NODE_ENV=production
COPY ./.env* ./
RUN npm run build
RUN cp ./.env.${NODE_ENV} ./build/buildinfo

FROM nginx:1.19.2-alpine AS final
RUN apk update --no-cache && apk upgrade --no-cache
COPY --from=base /app/build /usr/share/nginx/html
RUN rm -rf /etc/nginx/conf.d/default.conf
COPY ./config/nginx/default.conf /etc/nginx/conf.d/default.conf
ARG METRICS_PASSWORD=password
RUN apk add --no-cache apache2-utils \
  && htpasswd -b -c /etc/nginx/conf.d/.htpasswd metrics "${METRICS_PASSWORD}" \
  && apk del --purge apache2-utils
ENTRYPOINT ["nginx", "-g", "daemon off;"]
LABEL repo_url https://gitlab.com/codeprac/webapp/service
LABEL maintainer codeprac
